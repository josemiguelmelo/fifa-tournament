# FIFATournament

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)


FIFATournament is an web application for managing friendly FIFA games. This application was built as a personal project for storing all games results between me and my friends.

It creates some statistics for each player in the party so you can compare your skills against your friend's skills.

To build this application it was used Laravel PHP Framework, which official documentation can be found on the [Laravel website](https://laravel.com/docs/5.3), along with VueJS.

## Installation

In order to start using this application, you need to first create a database to store all data.

After having the database setup, you must set the information related to the database into the environment variable. 
Example:
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```

When database information is correctly set, run the migrations to create all the tables. Use this command:

```
php artisan migrate
```

Compile all javascript and sass files using this command:

```
gulp
```

After these steps, the application should be ready for use.


## API Endpoints

#### Get all matches made


```
GET  api/match
```

#### Store a new match on the database

```
POST  api/match
```

Params:

```
'player_1'       => Player 1 id,
'player_2'       => Player 2 id,
'player_1_goals' => Number of goals for player 1,
'player_2_goals' => Number of goals for player 2,
'player_1_team'  => Team used by Player 1,
'player_2_team'  => Team used by Player 2

```


#### Get list of all players


```
GET  api/players
```

#### Store a new player on the database

```
POST  api/players
```

Params:

```
'name'       => Player name

```


