
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

var Vue = require('vue');

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

Vue.component('add-player', require('./components/AddNewPlayer.vue'));
Vue.component('add-result', require('./components/AddNewResult.vue'));
Vue.component('player-list', require('./components/PlayersList.vue'));
Vue.component('results', require('./components/ResultsList.vue'));
Vue.component('classification-table', require('./components/ClassificationTable.vue'));
Vue.component('season', require('./components/Season.vue'));
Vue.component('seasons-winning-list', require('./components/seasons/SeasonsWinningList.vue'));

const app = new Vue({
    el: '#app',
    data : {
        allSeasonsResults : [],
        players: [],
        results : [],
        season : 1,
        seasons : [],
        page : 'main',
        pages : {
            'home' : 'main',
            'seasonsList' : 'seasons'
        }
    },
    mounted : function() {
        this.$http.get('/api/match/')
            .then(function(response) {
                // Get seasons
                this.seasons = Object.keys(response.data);
                this.season = Math.max.apply(null, this.seasons);
                // Get results for a season
                this.allSeasonsResults = response.data;
                this.results = this.allSeasonsResults[this.season];
            }, function(error) {
                console.log("ERROR ON MATCHES");
            });

        this.$http.get('/api/players/')
            .then(function(response) {
                this.players = response.data;
            }, function(error) {
                console.log("ERROR ON PLAYERS");
            });
    },
    methods : {
        newSeason : function() {
            this.season = parseInt(this.season) + 1;
            this.seasons.push(this.season);
            this.allSeasonsResults[this.season] = [];
            this.results = [];
        },
        changeSeason : function(seasonVal) {
            this.season = seasonVal;
            this.results = this.allSeasonsResults[this.season]
        },
        appendPlayer : function (player) {
            this.players.push(player);
        },
        appendMatch : function(match) {
            this.results.push(match);
        },

        changePage : function(page){
            this.page = page;
        }
    }
});

