<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FifaChallenge</title>

    <!-- Bootstrap Core CSS -->

    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/app.css" rel="stylesheet">

</head>

<body>

<div id="app">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="">FIFAChallenge</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#results" v-on:click="changePage('main')">Resultados</a>
                    </li>
                    <li>
                        <a v-on:click="changePage(pages['seasonsList'])">Seasons</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Full Width Image Header -->
    <header class="header-image">
        <div class="headline">
            <div class="container">
                <h1>FIFATournament</h1>
                <h2>Play well, earn well!</h2>
            </div>
        </div>
    </header>

    <!-- Page Content -->
    <div class="container">

        <div v-show="page == pages['home']">
            <div class="row">
                <div class="col-md-5">
                    <add-player v-on:player-added="appendPlayer"></add-player>
                </div>
                <div class="col-md-7">
                    <season :season="season" :seasons="seasons" v-on:change-season="changeSeason" v-on:new-season="newSeason()"></season>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <player-list :players="players" :results="results"></player-list>
                </div>
            </div>

            <hr class="featurette-divider">

            <div class="row" id="results">
                <div class="col-md-8">
                    <results :results="results" :players="players"></results>
                </div>
                <div class="col-md-4">
                    <add-result :players="players" :season="season" v-on:match-added="appendMatch"></add-result>
                </div>
            </div>

        </div>

        <div v-show="page == pages['seasonsList']">
            <div class="row">
                <div class="col-md-3">
                    <button type="button" class="btn btn-xs btn-warning" v-on:click="changePage(pages['home'])">
                        <i class="fa fa-arrow-left"></i> Go back
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11 col-md-offset-1">
                    <seasons-winning-list :players="players" :seasons="seasons" :matches="allSeasonsResults"></seasons-winning-list>
                </div>
            </div>

        </div>


        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; FIFATournament 2017</p>
                </div>
            </div>
        </footer>

    </div>
</div>

<!-- /.container -->

<!-- jQuery -->
<script>
    window.Laravel = <?php echo json_encode([
                                                'csrfToken' => csrf_token(),
                                            ]); ?>
</script>
<script src="js/app.js"></script>

</body>

</html>