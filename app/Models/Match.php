<?php

namespace FifaTournament\Models;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $fillable = [
        'player_1','player_2','player_1_goals','player_2_goals','player_1_team','player_2_team', 'season'
    ];

    public function getCreatedAtAttribute($value)
    {
        return explode(" ", $value)[0];
    }
}
