<?php

namespace FifaTournament\Models;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{

    protected $fillable = [
        'name'
    ];
    public function matches()
    {
        return $this->hasMany('FifaTournament\Models\Match');
    }
}
