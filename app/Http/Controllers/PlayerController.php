<?php

namespace FifaTournament\Http\Controllers;

use Carbon\Carbon;
use FifaTournament\Models\Player;
use Illuminate\Http\Request;

class PlayerController extends Controller
{
    public function index()
    {
        return Player::all();
    }

    public function store(Request $request)
    {
        if ( ! $request->has('name') || $request->get('name') == "")
            return json_encode(['error' => true, 'msg' => 'Name required.']);

        try{
            $player = Player::create(
                [
                    'name' => $request->get('name')
                ]);

            return json_encode(['error' => false, 'msg' => 'Player added successfully.', 'player' => $player]);
        } catch(\Exception $e)
        {
            return json_encode(['error' => true, 'msg' => 'Error saving player.', 'exception' => $e->getTraceAsString()]);
        }
    }
}
