<?php

namespace FifaTournament\Http\Controllers;

use FifaTournament\Models\Match;
use FifaTournament\Models\Player;
use Illuminate\Http\Request;

class MatchController extends Controller
{
    public function index()
    {
        return Match::all()->groupBy('season');
    }

    public function store(Request $request)
    {
        try {
            $match = Match::create(
                [
                    'player_1'       => $request->get('player_1'),
                    'player_2'       => $request->get('player_2'),
                    'player_1_goals' => $request->get('player_1_goals'),
                    'player_2_goals' => $request->get('player_2_goals'),
                    'player_1_team'  => $request->get('player_1_team'),
                    'player_2_team'  => $request->get('player_2_team'),
                    'season'         => $request->get('season'),
                ]);

            return json_encode(['error' => false, 'msg' => 'Match added successfully.', 'match' => $match]);
        } catch (\Exception $e) {
            return json_encode(['error' => true, 'msg' => 'Error saving match.', 'exception' => $e->getMessage()]);
        }
    }
}
