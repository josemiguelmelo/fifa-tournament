<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('player_1')->unsigned()->nullable();
            $table->foreign('player_1')->references('id')->on('players');
            $table->integer('player_2')->unsigned()->nullable();
            $table->foreign('player_2')->references('id')->on('players');

            $table->integer('player_1_goals')->unsigned()->default(0);
            $table->integer('player_2_goals')->unsigned()->default(0);

            // Team each player played with
            $table->string('player_1_team')->default("");
            $table->string('player_2_team')->default("");

            // Shots stats
            $table->integer('player_1_shots')->unsigned()->default(0);
            $table->integer('player_2_shots')->unsigned()->default(0);

            // Possession stats
            $table->integer('player_1_possession')->unsigned()->default(0);
            $table->integer('player_2_possession')->unsigned()->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
